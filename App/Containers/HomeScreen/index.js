import * as React from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet, FlatList } from 'react-native';
import { Helpers, } from '../../Theme'
export class HomeContainer extends React.Component {

  constructor(props) {
    super(props);
    this.state = { imageListData: null, imageList: [], selectedImageList: [], isListView: true, selectedImageListItem: [] }
  }

  componentDidMount() {
    fetch("https://jsonplaceholder.typicode.com/photos")
      .then(response => response.json())
      .then((response) => {
        let imageListData = {};
        response.forEach((imageItem) => {
          imageListData[imageItem.id] = imageItem;
        });
        this.setState({ imageList: response, imageListData: imageListData })
      }).catch(() => {

      })
  }

  handleAddCompareImage = (id) => {
    const { selectedImageList, selectedImageListItem } = this.state;
    this.setState({ selectedImageList: [...selectedImageList, id] })
  }

  isSelectedIteme = (id) => {
    return this.state.selectedImageList.indexOf(id) != -1 ? true : false
  }

  handleRemoveCompareImage = (id) => {
    let newselectedImageList = this.state.selectedImageList;
    let itemIndex = newselectedImageList.indexOf(id);
    newselectedImageList.splice(itemIndex, 1);
    this.setState({ selectedImageList: newselectedImageList })
  }

  handleToggleView = () => {
    this.setState({ isListView: !this.state.isListView })
  }

  render() {
    const { isListView } = this.state;
    let buttonTitle = this.state.isListView ? 'Compare' : 'Image Grid';
    let headingTitle = this.state.isListView ? 'Image Grid View' : 'Image COMPARISON View';
    return (
      <View style={styles.MainContainer}>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginVertical: 20, }}>
          <Text style={{ fontSize: 16, fontWeight: 'bold', textAlign: 'center' }}>{headingTitle}</Text>
          <TouchableOpacity style={[styles.button, { backgroundColor: 'red' }]} onPress={this.handleToggleView}>
            <Text style={styles.colorWhite}>{buttonTitle}</Text>
          </TouchableOpacity>
        </View>
        {!this.state.imageList.length && <View style={Helpers.center}><Text>Loading...</Text></View>}

        {isListView && <View>
          <FlatList
            data={this.state.imageList}
            renderItem={({ item, index }) => {
              const isSelected = this.isSelectedIteme(item.id)
              return (
                <View style={{ flex: 1, flexDirection: 'column', margin: 1, height: 250, borderWidth: 1, borderColor: '#000' }}>
                  <Image style={styles.imageThumbnail} source={{ uri: item.url }} />
                  <View style={[Helpers.center, { paddingHorizontal: 10 }]}>
                    <Text numberOfLines={1}>{item.title}</Text>
                    <Text numberOfLines={1}>{item.id}</Text>
                    <Text numberOfLines={1}>{item.url}</Text>
                    {!isSelected && <TouchableOpacity style={[styles.button, { backgroundColor: 'blue' }]} onPress={() => { this.handleAddCompareImage(item.id) }}>
                      <Text style={styles.colorWhite}>Compare</Text>
                    </TouchableOpacity>}
                    {isSelected && <TouchableOpacity style={[styles.button, { backgroundColor: 'red' }]} onPress={() => { this.handleRemoveCompareImage(item.id) }}>
                      <Text style={styles.colorWhite}>Remove</Text>
                    </TouchableOpacity>}
                  </View>
                </View>
              )
            }}
            //Setting the number of column
            numColumns={3}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>}
        {!isListView && <View>
          <View style={Helpers.center}>
            <Text>COMPARISON TABLE</Text>
          </View>
          <View style={{ flexDirection: 'row', borderWidth: 1, marginTop: 10 }}>
            <View style={{ flex: .30 }}></View>
            <View style={{ flex: .10, borderLeftWidth: 1, justifyContent: 'center', alignItems: 'center' }}><Text>ID</Text></View>
            <View style={styles.col3}><Text>URL</Text></View>
            <View style={styles.col3}><Text>Title</Text></View>
          </View>
          {!this.state.selectedImageList.length && <View style={Helpers.center}><Text>No selected item</Text></View>}
          <FlatList
            data={this.state.selectedImageList}
            renderItem={({ item, index }) => {
              const itemData = this.state.imageListData[item];
              return (
                <View style={{ flex: 1, flexDirection: 'row', margin: 1, borderWidth: 1, borderColor: '#000' }}>
                  <View style={{ flex: .30 }}><Image style={styles.imageThumbnail} source={{ uri: itemData.url }} /></View>
                  <View style={{ flex: .10, borderLeftWidth: 1, justifyContent: 'center', alignItems: 'center' }}><Text numberOfLines={1}>{itemData.id}</Text></View>
                  <View style={styles.col3}><Text numberOfLines={1}>{itemData.url}</Text></View>
                  <View style={styles.col3}><Text numberOfLines={1}>{itemData.title}</Text></View>
                </View>
              )
            }}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    // justifyContent: 'center',
    flex: 1,
    marginTop: 20,
    marginHorizontal: 5
  },
  imageThumbnail: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 100,
    marginHorizontal: 20
  },
  button: {
    marginTop: 10,
    width: 80,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  colorWhite: {
    color: 'white'
  },
  col2: { flex: .10, borderLeftWidth: 1, justifyContent: 'center', alignItems: 'center' },
  col3: { flex: .30, borderLeftWidth: 1, justifyContent: 'center', alignItems: 'center' }
});

export default HomeContainer;
