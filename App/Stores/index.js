import { combineReducers } from 'redux'
import configureStore from './CreateStore'
import rootSaga from 'App/Sagas'
import { reducer as form } from 'redux-form';

export default () => {
  const rootReducer = combineReducers({
    form
  })

  return configureStore(rootReducer, rootSaga)
}
