/**
 * Images should be stored in the `App/Images` directory and referenced using variables defined here.
 */

export default {
  logo: require('App/Assets/Images/TOM.png'),
  facebook: require('../Assets/Images/facebook-app-symbol.png'),
  google: require('../Assets/Images/google.png'),
  chevron_left: require('../Assets/Images/chevron-left.png'),
  property: require('../Assets/Images/annie-spratt-LRmOCObBzhk-unsplash.png'),
  homeActive: require('../Assets/Images/home.png'),
  homeInActive: require('../Assets/Images/home_in_active.png'),
  favInActive: require('../Assets/Images/fav_in_active.png'),
  searchesInActive: require('../Assets/Images/searches_in_active.png'),
  profileInActive: require('../Assets/Images/profile_in_active.png'),
  mapInActive: require('../Assets/Images/place_in_active.png'),
  mapActive: require('../Assets/Images/place.png'),
  listInActive: require('../Assets/Images/list_text_in_active.png'),
  listActive: require('../Assets/Images/list_text.png'),
  filter: require('../Assets/Images/filter_1.png'),
  search: require('../Assets/Images/search.png'),
  bed: require('../Assets/Images/bed.png'),
  bath: require('../Assets/Images/bath.png'),
  blueprint: require('../Assets/Images/blueprint.png'),
  view: require('../Assets/Images/view.png'),
  hidden: require('../Assets/Images/hidden.png'),
  favActive: require('../Assets/Images/fav_active.png'),
  favOutLineInActive: require('../Assets/Images/fav_outline_in_active.png'),
  flat: require('../Assets/Images/flat.png'),
  welcome1: require('../Assets/Images/welcome1.png')
}
