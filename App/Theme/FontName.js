import React from 'react';

module.exports = {
    Montserrat_Medium: 'Montserrat-Medium',
    Montserrat_SemiBold: 'Montserrat-SemiBold',
    Montserrat_Bold: 'Montserrat-Bold'
}